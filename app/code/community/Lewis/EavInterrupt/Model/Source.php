<?php

class Lewis_EavInterrupt_Model_Source extends Mage_Catalog_Model_Resource_Product_Indexer_Eav_Source {
	/*
	this method is functionally identical to parent::_prepareSelectIndex
	except for an additional left join in sub-selection onto the stock status
	table and an accompanying 'where in stock' clause, thereby excluding
	out-of-stock products from eav index tables.
	-anny@lewis
	-built for + tested in magento 1.7.0.2
	*/
	protected function _prepareSelectIndex($entityIds = null, $attributeId = null)
	{
		$adapter = $this->_getWriteAdapter();
		$idxTable = $this->getIdxTable();
		// prepare select attributes
		if (is_null($attributeId)) {
			$attrIds = $this->_getIndexableAttributes(false);
		} else {
			$attrIds = array($attributeId);
		}

		if (!$attrIds) {
			return $this;
		}

		/**@var $subSelect Varien_Db_Select*/
		$subSelect = $adapter->select()
			->from(
				array('s' => $this->getTable('core/store')),
				array('store_id', 'website_id')
			)
			->joinLeft(
				array('d' => $this->getValueTable('catalog/product', 'int')),
				'1 = 1 AND d.store_id = 0',
				array('entity_id', 'attribute_id', 'value')
			)
			// modifications begin here -anny@lewis
			->joinLeft(
				array('ss' => $this->getTable('cataloginventory/stock_status')),
				'd.entity_id=ss.product_id and s.website_id=ss.website_id',
				array()
			)
			->where('s.store_id != 0 and ss.stock_status = 1');
			// modifications end here -anny@lewis

		if (!is_null($entityIds)) {
			$subSelect->where('d.entity_id IN(?)', $entityIds);
		}

		/**@var $select Varien_Db_Select*/
		$select = $adapter->select()
			->from(
				array('pid' => new Zend_Db_Expr(sprintf('(%s)',$subSelect->assemble()))),
				array()
			)
			->joinLeft(
				array('pis' => $this->getValueTable('catalog/product', 'int')),
				'pis.entity_id = pid.entity_id AND pis.attribute_id = pid.attribute_id AND pis.store_id = pid.store_id',
				array()
			)
			->columns(
				array(
					'pid.entity_id',
					'pid.attribute_id',
					'pid.store_id',
					'value' => $adapter->getIfNullSql('pis.value', 'pid.value')
				)
			)
			->where('pid.attribute_id IN(?)', $attrIds);

		$select->where(Mage::getResourceHelper('catalog')->getIsNullNotNullCondition('pis.value', 'pid.value'));

		/**
		 * Add additional external limitation
		 */
		Mage::dispatchEvent('prepare_catalog_product_index_select', array(
			'select'		=> $select,
			'entity_field'  => new Zend_Db_Expr('pid.entity_id'),
			'website_field' => new Zend_Db_Expr('pid.website_id'),
			'store_field'   => new Zend_Db_Expr('pid.store_id')
		));

		$query = $select->insertFromSelect($idxTable);
		$adapter->query($query);

		return $this;
	}
}
